import requests
import os
import json
from lxml import html, etree
from lxml.html.clean import clean_html
from uuid import uuid4
from urllib import urlretrieve
from unicode_substitute import *

#URL SNIPPETS
url_snippet = { "veg_recipes_base" : "http://www.vegrecipesofindia.com/", "index" : "recipes-index/"}

#PATH CONSTANTS
PARENT_DIR = ".."
TEMP_DIR = os.path.abspath("temp/")
URLS_JSON_FILE = TEMP_DIR + "/recipe_urls.json"
FINAL_DIR = os.path.abspath("final/")
IMG_DIR = FINAL_DIR + "/img/"
JSON_DIR = FINAL_DIR + "/json/"

BROWSER_HEADERS = {'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0'}

def makedir(dirpath):
    if not (os.path.exists(dirpath)):
        os.mkdir(dirpath)

def checkedchangedir(dirpath):
    makedir(dirpath)
    os.chdir(dirpath)

def remove_nonsense(string):
    string = replace_fractions(string)
    return " ".join((string.encode('ascii', 'ignore')).split())

def remove_nonsense_list(list_name):
    new_list = []
    for string in list_name:
        new_list.append(remove_nonsense(string))
    new_list[:] = [string for string in new_list if any(char.isalnum() for char in string)]
    return new_list

def get_recipe_name(tree):
    return remove_nonsense("".join(tree.xpath("//div[@class = 'wprm-recipe-name wprm-color-header']//text()")))

def get_prep_time(tree):
    time_headers = remove_nonsense_list(tree.xpath("//div[@class = 'wprm-recipe-time-header']//text()"))
    time_vals = [remove_nonsense("".join(tree.xpath("(//div[@class = 'wprm-recipe-time'])[" + str(prep_index) + "]//text()"))) for prep_index in range(1, 4)]
    return dict(zip(time_headers, time_vals))

def get_recipe_summary(tree):
    return remove_nonsense("".join(tree.xpath("//div[@class = 'wprm-recipe-summary']//text()")))

def get_recipe_course(tree):
    return remove_nonsense("".join(tree.xpath("//span[@class = 'wprm-recipe-course']//text()")))

def get_recipe_cuisine(tree):
    return remove_nonsense("".join(tree.xpath("//span[@class = 'wprm-recipe-cuisine']//text()")))

def get_recipe_servings(tree):
    return remove_nonsense("".join(tree.xpath("//span[@itemprop = 'recipeYield']//input/@value")))

def get_recipe_ingredients(tree):
    ingredients_list = []
    total_ingredients = len(tree.xpath("//li[@class = 'wprm-recipe-ingredient']"))
    for each_ingredient in range(1, total_ingredients + 1):
        ingredients_list.append(remove_nonsense("".join(tree.xpath("(//li[@class = 'wprm-recipe-ingredient'])[" + str(each_ingredient) + "]//text()"))))
    return remove_nonsense_list(ingredients_list)

def get_recipe_instructions(tree):
    instructions_list = remove_nonsense_list(tree.xpath("//li[@class = 'wprm-recipe-instruction']//text()"))
    return instructions_list

def get_recipe_notes(tree):
    return remove_nonsense("".join(tree.xpath("//div[@class = 'wprm-recipe-notes-container']/p//text()")))

def get_recipe_dict(tree):
    recipe_dict = {}
    recipe_dict["Recipe name"] = get_recipe_name(tree)
    recipe_dict["Preparation time"] = get_prep_time(tree)
    recipe_dict["Summary"] = get_recipe_summary(tree)
    recipe_dict["Cuisine"] = get_recipe_cuisine(tree)
    #recipe_dict["Servings"] = get_recipe_servings(tree)
    recipe_dict["Ingredients"] = get_recipe_ingredients(tree)
    recipe_dict["Instructions"] = get_recipe_instructions(tree)
    notes = get_recipe_notes(tree)
    if notes:
        recipe_dict["Notes"] = get_recipe_notes(tree)
    return recipe_dict

def create_json_get_image(recipe_url):
    page = requests.get(recipe_url, headers = BROWSER_HEADERS)
    tree = html.fromstring(clean_html(page.content))
    if not tree.xpath("//div[@class = 'wprm-recipe-image']/img/@src"):
        return
    recipe_dict = get_recipe_dict(tree)
    image_id = str(uuid4())
    urlretrieve(tree.xpath("//div[@class = 'wprm-recipe-image']/img/@src")[0], "%s%s" % (IMG_DIR, image_id))
    recipe_dict["Image ID"] = image_id
    with open(JSON_DIR + recipe_dict["Recipe name"] + ".json", "w") as cuisine_fp:
        cuisine_fp.write(json.dumps(recipe_dict, indent = 2))

def get_each_cuisine():
    makedir(FINAL_DIR)
    makedir(IMG_DIR)
    makedir(JSON_DIR)
    recipe_urls = []
    with open(URLS_JSON_FILE) as urls_json_fp:
        recipe_urls = json.loads(urls_json_fp.read())
    for url in recipe_urls["urls"]:
        create_json_get_image(url)
        print url

if __name__ == "__main__":
    get_each_cuisine()
