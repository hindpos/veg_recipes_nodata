import re
fractions = {
    0x2189: "0",  # ; ; 0 # No       VULGAR FRACTION ZERO THIRDS
    0x2152: "1/10",  # ; ; 1/10 # No       VULGAR FRACTION ONE TENTH
    0x2151: "1/9",  # ; ; 1/9 # No       VULGAR FRACTION ONE NINTH
    0x215B: "1/8",  # ; ; 1/8 # No       VULGAR FRACTION ONE EIGHTH
    0x2150: "1/7",  # ; ; 1/7 # No       VULGAR FRACTION ONE SEVENTH
    0x2159: "1/6",  # ; ; 1/6 # No       VULGAR FRACTION ONE SIXTH
    0x2155: "1/5",  # ; ; 1/5 # No       VULGAR FRACTION ONE FIFTH
    0x00BC: "1/4",  # ; ; 1/4 # No       VULGAR FRACTION ONE QUARTER
    0x2153: "1/3",  # ; ; 1/3 # No       VULGAR FRACTION ONE THIRD
    0x215C: "3/8",  # ; ; 3/8 # No       VULGAR FRACTION THREE EIGHTHS
    0x2156: "2/5",  # ; ; 2/5 # No       VULGAR FRACTION TWO FIFTHS
    0x00BD: "1/2",  # ; ; 1/2 # No       VULGAR FRACTION ONE HALF
    0x2157: "3/5",  # ; ; 3/5 # No       VULGAR FRACTION THREE FIFTHS
    0x215D: "5/8",  # ; ; 5/8 # No       VULGAR FRACTION FIVE EIGHTHS
    0x2154: "2/3",  # ; ; 2/3 # No       VULGAR FRACTION TWO THIRDS
    0x00BE: "3/4",  # ; ; 3/4 # No       VULGAR FRACTION THREE QUARTERS
    0x2158: "4/5",  # ; ; 4/5 # No       VULGAR FRACTION FOUR FIFTHS
    0x215A: "5/6",  # ; ; 5/6 # No       VULGAR FRACTION FIVE SIXTHS
    0x215E: "7/8",  # ; ; 7/8 # No       VULGAR FRACTION SEVEN EIGHTHS
}

rx = r'(?u)(%s)' % '|'.join(map(unichr, fractions))

def replace_fractions(text):
    pattern = re.compile(rx)
    return pattern.sub(lambda x : fractions[ord(x.group())], text)
